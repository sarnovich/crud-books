//Definiendo clases
class Book {
    constructor(titulo, autor, id) {
        this.titulo = titulo;
        this.autor = autor;
        this.id =id;
    }
}

class UI {
    static mostrarLibros() {
        const books = Datos.traerLibros();
        console.log("boooks", books)
        books.forEach((book) => UI.agregarLibroLista(book))



    }

    static agregarLibroLista(book) {
       console.log(book.titulo)
        const lista = document.querySelector('#libro-list');
        
        const fila = document.createElement('tr');
        fila.innerHTML = `
        <td>${book.titulo}<td>
        <td>${book.autor}<td>
        <td>${book.id}<td>
        <td><a href ="#" class= "btn btn-danger btn-sm delete"> X </a> </td>
        `;
        lista.appendChild(fila);



    }

    static eliminarLibro(element){
        if (element.classList.contains('delete')) {
            element.parentElement.parentElement.remove()
        }

    }

    static mostrarAlerta(mensaje, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(mensaje));

        const container = document.getElementById("container")
        const form = document.getElementById("libro-form")
        console.log(div, form)
        container.insertBefore(div, form)

        setTimeout(()=> document.querySelector(".alert").remove(), 3000
        )

    }

    static limpiarCampos(){
        document.querySelector('#titulo').value=''
        document.getElementById("autor").value = ''
        document.getElementById("ID").value=''

    }

}

class Datos {
    static traerLibros(){
        let books;
        if (localStorage.getItem('libros') === null) {
            console.log("no tengo libros, creo array")
            books = [];
            
        } else {
            
            books = JSON.parse(localStorage.getItem('libros'));
            console.log("existe libro", books)
        }
        return books

    }
    static agregarUnLibro(book){
        const books = Datos.traerLibros();
        books.push(book);
        localStorage.setItem('libros', JSON.stringify(books))

    }
    static eliminarUnLibro(id){

    }
}

//agregar libros en la dom

document.addEventListener('DOMContentLoader',UI.mostrarLibros());

document.querySelector('#libro-form').addEventListener('submit',(e) => {
    e.preventDefault();
        //get data from form
        
    const titulo = document.getElementById("titulo").value;
    const autor = document.getElementById("autor").value;
    const id = document.getElementById("ID").value;
    
    if (titulo === '' || autor === '' || id === '') {
        UI.mostrarAlerta("mostrar los datos", 'danger')
        
    } else {
        const book = new Book(titulo, autor, id);
        Datos.agregarUnLibro(book)
        UI.agregarLibroLista(book)
        UI.mostrarAlerta("Libr agregado ok", "success")
        UI.limpiarCampos();
        
    }
})

document.getElementById('libro-list').addEventListener('click', (e) => {
    UI.eliminarLibro(e.target)
})